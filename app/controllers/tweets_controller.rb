class TweetsController < ApplicationController
  def index
    @tweets=Tweet.all
  end
  
  def new
    @tweet = Tweet.new
  end
  
  def create
    
    tweet =Tweet.new(message:params[:tweet][:message], tdate: Time.current)
    tweet.save
     redirect_to root_path
  end
   
  def destroy
     tweet = Tweet.find(params[:id])
     tweet.destroy
     redirect_to root_path
  end
  
  def show
    @tweet =Tweet.find(params[:id])
  end
  
  def edit
    @tweet = Tweet.find(params[:id])
  end
  
  def update
    tweet = Tweet.find(params[:id])
    tweet.message=params[:tweet][:message]
    tweet.save
    redirect_to root_path
  end
end
